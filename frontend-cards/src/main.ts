import { createApp } from 'vue'

import { library } from '@fortawesome/fontawesome-svg-core'
import { faCheckCircle, faTimesCircle } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

import App from './App.vue'

import Home from './views/HomeView.vue'
import Card from './views/CardView.vue'
import { createRouter, createWebHashHistory } from 'vue-router'

const router = createRouter({
  history: createWebHashHistory(),
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/:oldId',
      name: 'cards updated',
      component: Home
    },
    {
      path: '/affiliate/:affiliate_id',
      name: 'individual card',
      component: Card
    },
    {
      path: '/individual/:individual_id',
      name: 'affiliate card',
      component: Card
    }
  ]
})

library.add(faCheckCircle, faTimesCircle)

createApp(App)
  .use(router)
  .component('FaIcon', FontAwesomeIcon)
  .mount('#app')
