FROM node:16-alpine
ADD ./frontend-emmi .
RUN npm ci
RUN npm run build

FROM node:16-alpine
ADD ./frontend-cards .
RUN npm ci
RUN npm run build

FROM node:16-alpine
ENV NODE_ENV="production"
WORKDIR /emmi
ADD ./backend /emmi
RUN npm ci
COPY --from=0 /dist /emmi/public_html_emmi
COPY --from=1 /dist /emmi/public_html_cards
CMD ["npm", "start"]