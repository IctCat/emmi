import { DateTime } from 'luxon'

export function formatDate (date: Date, formatString: string = 'MMMM yyyy') {
  return DateTime.fromJSDate(date).toFormat(formatString)
}
