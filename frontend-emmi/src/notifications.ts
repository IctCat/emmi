import Vue from 'vue'

export function showSuccess (title: string, text?: string) {
  // @ts-ignore Vue 2.x magic
  Vue.notify({
    group: 'success',
    type: 'success',
    title,
    text,
  })
}

export function showError (title: string, text?: string) {
  // @ts-ignore Vue 2.x magic
  Vue.notify({
    group: 'error',
    type: 'error',
    title,
    text,
    duration: -1
  })
}
