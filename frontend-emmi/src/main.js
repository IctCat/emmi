// Vue 2 - The magic
import Vue from 'vue'

// Pinia for state management
import { createPinia, PiniaVuePlugin } from 'pinia'

// Bootstrap 4 - UX framework
import BootstrapVue from 'bootstrap-vue'
import 'bootstrap/scss/bootstrap.scss'
import './theme/darkly-bootstrap.min.css'
// import './theme/_variables.scss'
// import './theme/_bootswatch.scss'
import 'bootstrap-vue/dist/bootstrap-vue.css'

// Axios - HTTP library
import axios from 'axios'

// VueCookies - Cookie library
import VueCookies from 'vue-cookies'

// VueNotifications - Toaster library
import Notifications from 'vue-notification'

// Font Awesome - free icon library
import { library } from '@fortawesome/fontawesome-svg-core'
import { faCalendarAlt, faCalendarCheck, faTags, faWrench, faSpinner, faPlus, faStar, faCheck, faClock, faTimes, faChevronCircleRight, faChevronCircleDown, faCity, faUsers, faUserTag, faGraduationCap } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

// Load Script - generic JS runtime injector
import LoadScript from 'vue-plugin-load-script'

// Our own stuff
import router from './router'
import App from './App.vue'
import TopNavigation from './components/TopNavigation.vue'
import LoadingSpinner from './components/LoadingSpinner.vue'
import DevDataDump from './components/DevDataDump.vue'
import RawData from './components/RawData.vue'

Vue.use(BootstrapVue)
Vue.use(VueCookies)
Vue.use(Notifications)
Vue.use(LoadScript)
Vue.use(PiniaVuePlugin)
const pinia = createPinia()

library.add(faCalendarAlt, faCalendarCheck, faTags, faWrench, faSpinner, faPlus, faStar, faCheck, faClock, faTimes, faChevronCircleRight, faChevronCircleDown, faCity, faUsers, faUserTag, faGraduationCap)

// Global components
Vue.component('FaIcon', FontAwesomeIcon)
Vue.component('TopNavigation', TopNavigation)
Vue.component('LoadingSpinner', LoadingSpinner)
Vue.component('DevDataDump', DevDataDump)
Vue.component('RawData', RawData)

Vue.config.productionTip = false

// Turn Vue on and go!
new Vue({
  router,
  pinia,
  render: h => h(App),
}).$mount('#app')
