export type EmmiUser = {
  _id: string
  name: string
  role: number
  roleName: string
}

export type Visitor = {
  _id?: string
  firstName: string
  lastName: string
  email: string
  organization: string
  lastUpdated: Date
  checkedIn?: boolean
  oldEmail?: string
  isLoading?: boolean // Used to block UI during checkin
}

export type IndividualMember = {
  _id?: string
  membershipType: string,
  firstName: string,
  lastName: string,
  email: string,
  postOffice: string,
  createdDate: Date,
  lastUpdated: Date,
  lastRenewed: Date,
  validUntil: Date,
  igdaFinlandId: number,
  igdaOrgId: string,
  notes: string,
  token: string
}

export type IndividualMemberCard = {
  firstName: string
  lastName: string
  createdDate: Date
  validUntil: Date
  membershipType: string
  igdaFinlandId: number | string
}

export type Affiliate = {
  name: string,
  contact: {
    name: string,
    email: string
  },
  createdDate: Date
  organisationType: string
  postOffice: string,
  validUntil: Date,
  notes: string,
  memberIdPrefix: string,
  members: {
    firstName: string,
    lastName: string,
    email: string,
    idSuffix: number,
    hasGivenOrgPermission: boolean,
    fullId: string
  }[]
}

export type Event = {
  _id?: string
  name: string
  location: string
  organizer: string
  sponsor: string
  notes: string
  date: Date
  visitors: {
    id: string
    checkin: Date
  }[]
  visitorCountOverride: number
  visitorCountOffset: number
}

export type EventHeadline = {
  _id: string
  name: string
  location: string
  organizer: string
  sponsor: string
  date: Date
}

export type NewVisitor = Visitor & {
  interests: {
    [key: string]: boolean
  }
}

export type MCGroup = {
  name: string
  id: string
}
