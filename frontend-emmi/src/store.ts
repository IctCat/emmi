import { defineStore } from 'pinia'
import { ref } from 'vue'

export const useStore = defineStore('main', () => {
  const isProductionEnv = ref(true)
  const isLoggedIn = ref(false)
  const userRole = ref(0)
  const username = ref('')
  const roleName = ref('')
  const version = ref('🤔')

  return {
    isProductionEnv,
    isLoggedIn,
    userRole,
    username,
    roleName,
    version,
  }
})
