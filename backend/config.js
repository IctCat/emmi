// Note: this is not a smart or sophisticated way of doing configs, but it sure works for small scale. You need to have a valid secrets.json and google key file in docker secrets to connect to our live services. A future action might be to docker in a vanilla dev db for convenience?

const pjson = require('./package.json')

const config = {
  productionEnv: false,
  version: pjson.version,
  // eslint-disable-next-line import/no-absolute-path
  secrets: require('/run/secrets/backend_secrets.json'),
  cron: {
    membershipEmailsString: '00 00 01 01 * *' // On the 1st of every month at 01:00
  },
  nodemailer: {
    prodConfig: {
      pool: true,
      debug: false,
      rateDelta: 1000 * 60 * 60, // Rate limiting resolution to 1 hour
      rateLimit: 100 // Mailgun probation accounts set to 100 mails per hour until lifted
    },
    devConfig: {
      debug: true
    },
    from: {
      members: 'IGDA Finland Memberships <members@igda.fi>',
      webmaster: 'IGDA Finland Webmaster <webmaster@igda.fi>'
    }
  }
}

module.exports = config
