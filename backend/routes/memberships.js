// Users don't know or care about their membership type.
// This file houses actions that on the surface should work the same for individuals and affiliates

const emmiApi = require('./dbapi')
// const config = require('../server').config
const mongoose = require('mongoose')
const individualMemberSchema = emmiApi.individualMemberSchema
const Member = mongoose.model('IndividualMember', individualMemberSchema)
const affiliateSchema = emmiApi.affiliateSchema
const Affiliate = mongoose.model('Affiliate', affiliateSchema)
const nodemailer = emmiApi.nodemailer
// const moment = require('moment')
const slack = require('../slack')
const individualMember = require('./individualMember')

module.exports = {
  getIndividualCard: async function (req, res) {
    try {
      let member = await Member.findOne({ _id: req.params.id }, 'firstName lastName createdDate validUntil membershipType igdaFinlandId')
      if (!member) res.status(400).send('Member not found')
      res.json(member)
    } catch (err) {
      res.status(500).send(err)
    }
  },

  getAffiliateCard: async function (req, res) {
    try {
      let affiliate = await Affiliate.findOne({ 'members._id': req.params.id }, { createdDate: 1, validUntil: 1, name: 1, members: { $elemMatch: { _id: req.params.id } } })
      if (!affiliate || affiliate.members.length === 0) res.status(400).send('Member not found')
      else {
        // Construct a response
        res.json({
          firstName: affiliate.members[0].firstName,
          lastName: affiliate.members[0].lastName,
          createdDate: affiliate.createdDate,
          validUntil: affiliate.validUntil,
          affiliateName: affiliate.name,
          membershipType: 'Affiliate',
          igdaFinlandId: affiliate.members[0].fullId
        })
      }
    } catch (err) {
      res.status(400).send('Member not found')
    }
  },

  emailLinks: function (req, res) {
    Member.findOne({ 'email': req.body.email }, '_id firstName email token igdaFinlandId', (err, member) => {
      // If not found...
      if (err || member === null) {
        // ...look for an affiliate instead
        Affiliate.find({ // Should find all instead of one and sort
          'members.email': req.body.email
        }, {
          createdDate: 1,
          validUntil: 1,
          name: 1,
          memberIdPrefix: 1,
          members: {
            $elemMatch: {
              email: req.body.email
            }
          }
        }, {
          sort: { validUntil: -1 }
        }, (err, affiliates) => {
          if (err) res.status(500).send(err)
          else if (affiliates === null) res.status(400).send('Email not found')
          else {
            if (affiliates.length === 0) res.status(400).send('Email not found')
            else if (affiliates[0].members.length === 0) res.status(400).send('Email not found')
            else {
              affiliates[0].createdDate = new Date(affiliates[0].createdDate)
              nodemailer.sendAffiliateLinksEmail(affiliates[0], (err) => {
                if (err) res.status(500).send(err)
                else res.send('ok')
              })
            }
          }
        })
      } else {
        if (member.token != null) {
          nodemailer.sendIndividualLinksEmail(member, (err) => {
            if (err) res.status(500).send(err)
            else res.send('ok')
          })
        } else {
          // No token -> create
          individualMember.saveNewEditToken(member._id, (err, newToken) => {
            if (err) {
              console.error('Error generating a new edit token for email: ' + err)
              slack.post('Error generating a new edit token for email:\n' + err, false)
              res.status(500).send(err)
            } else {
              member.token = newToken
              nodemailer.sendIndividualLinksEmail(member, (err) => {
                if (err) res.status(500).send(err)
                else res.send('ok')
              })
            }
          })
        }
      }
    })
  },

  emailCheck: function (req, res) {
    req.body.email = req.body.email.toLowerCase().trim() // Just to be sure...
    Member.findOne({ 'email': req.body.email }, '_id', function (err, member) {
      if (err) res.status(500).send(err)
      else if (member) res.send('used')
      else {
        Affiliate.findOne({ 'members.email': req.body.email }, '_id', (err, affiliate) => {
          if (err) res.status(500).send(err)
          else if (affiliate) res.send('used')
          else res.send('free')
        })
      }
    })
  }
}
