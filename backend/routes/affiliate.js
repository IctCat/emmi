const emmiApi = require('./dbapi')
// const config = require('../server').config
const mongoose = require('mongoose')
const affiliateSchema = emmiApi.affiliateSchema
const Affiliate = mongoose.model('Affiliate', affiliateSchema)
// const nodemailer = emmiApi.nodemailer
// const moment = require('moment')
const slack = require('../slack')
// eslint-disable-next-line no-unused-vars
const csv = require('csv-express')

module.exports = {
  getAll: function (req, res) {
    Affiliate.find(function (err, affiliates) {
      if (err) {
        res.status(500).send(err)
      } else {
        res.json(affiliates)
      }
    })
  },

  getAllAsCsv: function (req, res) {
    Affiliate.find(function (err, affiliates) {
      if (err) {
        res.status(500).send(err)
      } else {
        let response = []
        for (let a of affiliates) {
          a.validUntil = new Date(a.validUntil)

          for (let m of a.members) {
            response.push({
              firstName: m.firstName,
              lastName: m.lastName,
              email: m.email,
              id: m.fullId,
              validUntil: `${a.validUntil.getFullYear()}-${a.validUntil.getMonth() + 1}-${a.validUntil.getDate() + 1}`
            })
          }
        }
        res.csv(response, true)
      }
    })
  },

  getAllValid: function (req, res) {
    Affiliate.find({ validUntil: { $gte: new Date() } }, function (err, affiliates) {
      if (err) {
        res.status(500).send(err)
      } else {
        res.json(affiliates)
      }
    })
  },

  getAllValidAsCsv: function (req, res) {
    Affiliate.find({ validUntil: { $gte: new Date() } }, function (err, affiliates) {
      if (err) {
        res.status(500).send(err)
      } else {
        let response = []
        for (let a of affiliates) {
          a.validUntil = new Date(a.validUntil)

          for (let m of a.members) {
            response.push({
              firstName: m.firstName,
              lastName: m.lastName,
              email: m.email,
              id: m.fullId,
              validUntil: `${a.validUntil.getFullYear()}-${a.validUntil.getMonth() + 1}-${a.validUntil.getDate() + 1}`
            })
          }
        }
        res.csv(response, true)
      }
    })
  },

  getById: function (req, res) {
    Affiliate.findById(req._id, function (err, affiliate) {
      if (err) {
        res.status(500).send(err)
      } else {
        res.json(affiliate)
      }
    })
  },

  getByIdAsCsv: async function (req, res) {
    let affiliate = await Affiliate.findById(req._id).lean()
    res.csv(affiliate.members, true)
  },

  create: function (req, res) {
    req.body.contact.email = req.body.contact.email.toLowerCase().trim() // Just to be sure...
    let newAffiliate = new Affiliate(req.body)
    for (let i = 0; i < newAffiliate.members.length; i++) {
      newAffiliate.members[i].fullId = newAffiliate.memberIdPrefix + newAffiliate.createdDate.getFullYear().toString().slice(-2) + '-' + req.body.members[i].idSuffix
    }

    // No validation for dates because these are often manually adjusted anyway

    // Create
    newAffiliate.save(function (err, result) {
      if (err) {
        res.status(500).send(err)
      } else {
        res.json(result)
      }
    })
  },

  updateById: function (req, res) {
    req.body.contact.email = req.body.contact.email.toLowerCase().trim() // Just to be sure...
    let newData = req.body
    for (let i = 0; i < newData.members.length; i++) {
      newData.members[i].fullId = newData.memberIdPrefix + new Date(newData.createdDate).getFullYear().toString().slice(-2) + '-' + req.body.members[i].idSuffix
    }
    newData.lastUpdated = new Date()

    Affiliate.findByIdAndUpdate(req._id, { $set: newData }, { 'new': true }, function (err, updatedAffiliate) {
      if (err) {
        res.status(500).send(err)
      } else {
        res.json(updatedAffiliate)
      }
    })
  },

  addMembersById: function (req, res) {
    Affiliate.findById(req._id, function (err, affiliate) {
      if (err) {
        res.status(500).send(err)
      } else {
        affiliate = new Affiliate(affiliate)
        let nextIdSuffix = affiliate.members.length + 1
        affiliate.createdDate = new Date(affiliate.createdDate)

        for (let member of req.body) {
          member.email = member.email.toLowerCase().trim() // Just to be sure...
          member.idSuffix = nextIdSuffix
          member.fullId = affiliate.memberIdPrefix + affiliate.createdDate.getFullYear().toString().slice(-2) + '-' + member.idSuffix
          nextIdSuffix++
          affiliate.members.push(member)
        }

        affiliate.save(function (err, result) {
          if (err) res.status(500).send(err)
          else {
            slack.post(req.body.length.toString() + ' members added to ' + affiliate.name + ' ' + affiliate.createdDate.getFullYear().toString(), true)
            res.json(result)
          }
        })
      }
    })
  },

  updateMemberById: function (req, res) {
    Affiliate.findById(req._id, function (err, affiliate) {
      if (err) {
        res.status(500).send(err)
      } else {
        affiliate = new Affiliate(affiliate)
        req.body.email = req.body.email.toLowerCase().trim() // Just to be sure...
        for (let i = 0; i < affiliate.members.length; i++) {
          if (affiliate.members[i].idSuffix === req.body.idSuffix) {
            req.body.fullId = affiliate.memberIdPrefix + affiliate.createdDate.getFullYear().toString().slice(-2) + '-' + req.body.idSuffix
            affiliate.members[i] = req.body
          }
        }

        affiliate.save(function (err, result) {
          if (err) res.status(500).send(err)
          else res.json(result)
        })
      }
    })
  },

  deleteMemberById: function (req, res) {
    Affiliate.findById(req._id, function (err, affiliate) {
      if (err) {
        res.status(500).send(err)
      } else {
        affiliate = new Affiliate(affiliate)

        for (let i = 0; i < affiliate.members.length; i++) {
          if (affiliate.members[i].idSuffix === req.body.idSuffix) {
            affiliate.members.splice(i, 1)
          }
        }
        // Redistribute IDs
        for (let i = 0; i < affiliate.members.length; i++) {
          affiliate.members[i].idSuffix = i + 1
          affiliate.members[i].fullId = affiliate.memberIdPrefix + affiliate.createdDate.getFullYear().toString().slice(-2) + '-' + req.body.members[i].idSuffix
        }

        affiliate.save(function (err, result) {
          if (err) res.status(500).send(err)
          else res.json(result)
        })
      }
    })
  },

  deleteById: function (req, res) {
    Affiliate.findByIdAndRemove(req._id, function (err, affiliate) {
      if (err) {
        res.status(500).send(err)
      } else {
        res.status(200).send()
      }
    })
  }
}
