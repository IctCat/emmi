const express = require('express')
const router = express.Router()
const bodyParser = require('body-parser')
const multer = require('multer')
const mongoose = require('mongoose')
const server = require('../server')
const app = server.app
const nodemailer = server.nodemailer
const mailchimp = server.mailchimp
const upload = multer()

const eventOrganizerSchema = mongoose.Schema({
  name: String
})

const eventSchema = mongoose.Schema({
  name: String,
  location: String,
  organizer: String,
  sponsor: String,
  notes: String,
  date: {
    type: Date,
    default: Date.now()
  },
  visitors: [{
    id: String,
    checkin: {
      type: Date,
      default: Date.now()
    }
  }],
  visitorCountOverride: {
    type: Number,
    default: 0
  },
  visitorCountOffset: {
    type: Number,
    default: 0
  }
})

const visitorSchema = mongoose.Schema({
  firstName: String,
  lastName: String,
  email: String,
  organization: String,
  lastUpdated: Date
})

const individualMemberSchema = mongoose.Schema({
  membershipType: String,
  firstName: String,
  lastName: String,
  email: String,
  postOffice: String,
  createdDate: { type: Date, default: Date.now() },
  lastUpdated: { type: Date, default: Date.now() },
  lastRenewed: Date,
  validUntil: Date,
  igdaFinlandId: Number,
  igdaOrgId: String,
  notes: String,
  token: String
})

const affiliateSchema = mongoose.Schema({
  name: String,
  contact: { name: String, email: String },
  createdDate: { type: Date, default: Date.now() },
  organisationType: { type: String, default: 'Business' },
  postOffice: String,
  validUntil: Date,
  notes: String,
  memberIdPrefix: String,
  members: [{
    firstName: String,
    lastName: String,
    email: String,
    idSuffix: Number,
    hasGivenOrgPermission: Boolean,
    fullId: String
  }]
})

module.exports = {
  router,
  eventOrganizerSchema,
  eventSchema,
  visitorSchema,
  individualMemberSchema,
  affiliateSchema,
  mailchimp,
  nodemailer
}

const individualMember = require('./individualMember')
const visitor = require('./visitor')
const affiliate = require('./affiliate')
const memberships = require('./memberships')
const event = require('./event')
const pAuth = require('./pauth')

module.exports.sendMonthlyEmails = individualMember.sendMonthlyEmails

function requireLogin (req, res, next) {
  if (req.isAuthenticated()) return next()
  res.clearCookie('user')
  res.status(401).send('Not authenticated')
}

// eslint-disable-next-line no-unused-vars
function requireVolunteerRole (req, res, next) {
  if (req.session.passport.user && req.session.passport.user.role >= 2) return next()
  res.status(401).send('Not enough street cred')
}

function requireBoardRole (req, res, next) {
  if (req.session.passport.user && req.session.passport.user.role >= 3) return next()
  res.status(401).send('Not enough street cred')
}

function requireAdminRole (req, res, next) {
  if (req.session.passport.user && req.session.passport.user.role >= 4) return next()
  res.status(401).send('Not enough street cred')
}

function requireDeveloperRole (req, res, next) {
  if (req.session.passport.user && req.session.passport.user.role >= 5) return next()
  res.status(401).send('Not enough street cred')
}

function requireEditToken (req, res, next) {
  let token
  let _id
  if (req.method !== 'GET') {
    if (req.body.token != null) {
      token = req.body.token
      _id = req.body._id
    } else { res.status(401).send() }
  } else {
    token = req.params._token
    _id = req.params._id
  }

  if (token == null || _id == null) { res.status(401).send() } else if (individualMember.isTokenValid(_id, token, function (err) {
    if (!err) { next() } else { res.status(401).send('Unauthorised: ' + err) }
  }));
}

// Base route (unsecure)
// ==========

router.get('/', function (req, res) {
  res.json({ message: 'Tere tulemast!' })
})

// Routes for membership (unsecure)
// ==========

router.post('/individual-members/checkout-webhook',
  bodyParser.raw({ type: 'application/json' }), // Speacial parsing for Stripe signature
  individualMember.checkout)

// Default parsing for all rountes after this line
router.use(bodyParser.json())
router.use(bodyParser.urlencoded({ extended: true }))
router.use(upload.array())

router.route('/individual-members/editWithToken/:_id/:_token')
  .all(requireEditToken)
  .get(individualMember.getById)
  .put(individualMember.updateWithToken)

router.route('/cards/emailcheck')
  .post(memberships.emailCheck)

router.route('/cards/get-link')
  .post(memberships.emailLinks)

router.route('/cards/individual/:id')
  .get(memberships.getIndividualCard)

router.route('/cards/affiliate/:id')
  .get(memberships.getAffiliateCard)

// Route for env check (unsecure)
// ==========

router.get('/state', function (req, res) {
  const response = {
    env: app.get('env'),
    version: server.config.version
  }
  if (req.isAuthenticated()) {
    response.user = {
      name: req.session.passport.user.name,
      role: req.session.passport.user.role,
      roleName: req.session.passport.user.roleName
    }
  }
  res.json(response)
})

// Route for Passport authentication (unsecure)
// ==========

router.get('/auth/', pAuth.getUser)

router.get('/auth/login', pAuth.login)

router.get('/auth/login/callback', pAuth.loginCallback1, pAuth.loginCallback2)

router.get('/auth/logout', pAuth.logout)

// Require authentication for all routes registered after this line
router.use(requireLogin)

// Route parameters
// ================

router.param('_id', function (req, res, next, id) {
  req._id = id
  next()
})

// Individual member route
// =============

router.route('/individual-members/cards')
  .get(individualMember.getAllCards)

router.route('/individual-members/cards/valid')
  .get(individualMember.getAllValidCards)

router.route('/individual-members')
  .all(requireBoardRole)
  .get(individualMember.getAll)

router.route('/individual-members/count')
  .all(requireBoardRole)
  .get(individualMember.getCount)

router.route('/individual-members/csv')
  .all(requireBoardRole)
  .get(individualMember.getAllCsv)

router.route('/individual-members/valid/csv')
  .all(requireBoardRole)
  .get(individualMember.getAllValidAsCsv)

router.route('/individual-members/:_id')
  .all(requireBoardRole)
  .get(individualMember.getById)
  .put(individualMember.update)
  .all(requireAdminRole)
  .delete(individualMember.delete)

router.route('/individual-members/new')
  .all(requireBoardRole)
  .post(individualMember.create)

router.route('/individual-members/getToken/:_id')
  .all(requireBoardRole)
  .get(individualMember.getEditToken)

// Affiliate route
// =============

router.route('/affiliates')
  .all(requireBoardRole)
  .get(affiliate.getAll)

router.route('/affiliates/csv')
  .all(requireBoardRole)
  .get(affiliate.getAllAsCsv)

router.route('/affiliates/valid')
  .get(affiliate.getAllValid)

router.route('/affiliates/valid/csv')
  .get(affiliate.getAllValidAsCsv)

router.route('/affiliates/new')
  .all(requireBoardRole)
  .post(affiliate.create)

router.route('/affiliates/:_id')
  .all(requireBoardRole)
  .get(affiliate.getById)
  .put(affiliate.updateById)
  .all(requireAdminRole)
  .delete(affiliate.deleteById)

router.route('/affiliates/:_id/csv')
  .all(requireBoardRole)
  .get(affiliate.getByIdAsCsv)

router.route('/affiliates/:_id/members')
  .all(requireBoardRole)
  .post(affiliate.addMembersById)

router.route('/affiliates/:_id/members/:idSuffix')
  .all(requireBoardRole)
  .put(affiliate.updateMemberById)
  .delete(affiliate.deleteMemberById)

// Visitor route
// =============

router.route('/visitors')
  .all(requireVolunteerRole)
  .get(visitor.getAll)

router.route('/visitors/count')
  .all(requireVolunteerRole)
  .get(visitor.getCount)

router.route('/visitors/mailchimp')
  .all(requireVolunteerRole)
  .post(visitor.getMailchimp)

router.route('/visitors/mailchimp/groups')
  .all(requireVolunteerRole)
  .get(visitor.getMailchimpGroups)

router.route('/visitors/csv')
  .all(requireVolunteerRole)
  .get(visitor.getAllCsv)

router.route('/visitors/new')
  .all(requireVolunteerRole)
  .post(visitor.create)

router.route('/visitors/:_id')
  .all(requireVolunteerRole)
  .get(visitor.getById)
  .put(visitor.update)
  .delete(visitor.delete)

// Event route
// ===========

router.route('/events')
  .all(requireVolunteerRole)
  .get(event.getAll)

router.route('/events/headlines')
  .all(requireVolunteerRole)
  .get(event.getAllHeadlines)

router.route('/events/organizers')
  .all(requireVolunteerRole)
  .get(event.getAllOrganizers)

router.route('/events/organizers/add')
  .all(requireAdminRole)
  .post(event.createOrganizer)

router.route('/events/organizers/:_id')
  .all(requireAdminRole)
  .put(event.updateOrganizer)
  .delete(event.deleteOrganizer)

router.route('/events/new')
  .all(requireVolunteerRole)
  .post(event.create)

router.route('/events/:_id')
  .all(requireVolunteerRole)
  .get(event.getById)
  .put(event.update)
  .delete(event.delete)

router.route('/events/:_id/visitors')
  .all(requireVolunteerRole)
  .get(event.getVisitors)

router.route('/events/:_id/visitors/add')
  .all(requireVolunteerRole)
  .post(event.addVisitor)

router.route('/events/:_id/visitors/remove')
  .all(requireVolunteerRole)
  .post(event.removeVisitor)

// MailChimp route
// ===========
/*
router.route('/mailchimp/lists')
    .get(mailchimp.getAllLists);

router.route('/mailchimp/lists/members')
    .get(mailchimp.getMemberListDetails);

router.route('/mailchimp/lists/visitors')
    .get(mailchimp.getVisitorListDetails);

router.route('/mailchimp/lists/members/members')
    .get(mailchimp.getMemberListMembers);

router.route('/mailchimp/lists/visitors/members')
    .get(mailchimp.getVisitorListMembers);

router.route('/mailchimp/sync')
    .get(mailchimp.syncToMailChimp);
*/
// Other
// =============

router.route('/roles')
  .all(requireAdminRole)
  .get(pAuth.getAllUsers)

router.route('/roles/admins')
  .get(pAuth.getAllAdmins)

router.route('/roles/:_id')
  .all(requireAdminRole)
  .put(pAuth.setUser)
  .delete(pAuth.deleteUser)

router.route('/debug/email')
  .all(requireDeveloperRole)
  .post(nodemailer.sendDebugEmail)

router.route('/debug/monthly-emails')
  .all(requireDeveloperRole)
  .get(function (req, res) {
    individualMember.sendMonthlyEmails()
    res.send('ok')
  })

router.route('/debug/cron/monthly-emails')
  .all(requireDeveloperRole)
  .get(function (req, res) {
    individualMember.sendMonthlyEmails()
    res.send('ok')
  })

router.use(function (req, res, next) {
  res.status(404).json({ message: 'Not found :(' })
})
