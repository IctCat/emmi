const mongoose = require('mongoose')
const api = require('./dbapi')
const visitorSchema = api.visitorSchema
const Visitor = mongoose.model('Visitor', visitorSchema)
const mailchimp = api.mailchimp
const server = require('../server')
const config = server.config

module.exports = {
  getCount: function (req, res) {
    Visitor.countDocuments({}, function (err, data) {
      if (err) {
        res.status(500).send(err)
      } else {
        res.json({ count: data })
      }
    })
  },

  getAll: function (req, res) {
    Visitor.find(function (err, visitors) {
      if (err) {
        res.status(500).send(err)
      } else {
        res.json(visitors)
      }
    })
  },

  getAllCsv: function (req, res) {
    const query = Visitor.find()
    query.select('-_id firstName lastName email organization')
    // query.where('email').gt(2);
    query.lean()
    query.exec(function (err, visitors) {
      if (err) {
        res.status(500).send(err)
      } else {
        res.csv(visitors)
      }
    })
  },

  getById: function (req, res) {
    Visitor.findById(req._id, function (err, visitor) {
      if (err) {
        res.status(500).send(err)
      } else {
        res.json(visitor)
      }
    })
  },

  getMailchimp: function (req, res) {
    mailchimp.GetVisitor(req.body.email).then(function (result) {
      res.json(result)
    }).catch(function (err) {
      if (err.status === 404) {
        res.json({ status: 'not found' })
      } else {
        console.error('Error fetching visitor mailchimp info: ' + JSON.stringify(err, null, 4))
        res.status(500).send(err)
      }
    })
  },

  getMailchimpGroups: function (req, res) {
    mailchimp.GetInterests().then(function (result) {
      res.json(result)
    }).catch(function (err) {
      res.status(500).send(err)
    })
  },

  create: function (req, res) {
    if (req.body.hasOwnProperty('email')) req.body.email = req.body.email.toLowerCase().trim() // Just to be sure...
    const visitor = new Visitor(req.body)
    visitor.lastUpdated = Date.now()

    visitor.save(function (err, vis) {
      if (err) {
        res.status(500).send(err)
      } else {
        if (config.productionEnv) {
          mailchimp.AddVisitor(req.body).then(function (result) {
            res.json(vis)
          }).catch(function (err) {
            if (err.status === 400) {
              console.log("MailChimp rejected '" + visitor.email + "'. Oh well. *shrug*")
              res.json(vis)
            } else {
              console.error('Error creating a new visitor: ' + JSON.stringify(err, null, 4))
              res.status(500).send(err)
            }
          })
        } else res.json(vis)
      }
    })
  },

  update: function (req, res) {
    const newValues = {
      firstName: req.body.firstName,
      lastName: req.body.lastName,
      email: req.body.email.toLowerCase().trim() || '',
      organization: req.body.organization || '',
      lastUpdated: new Date()
    }

    Visitor.findByIdAndUpdate(req._id, { $set: newValues }, function (err, updatedVisitor) {
      if (err) {
        res.status(500).send(err)
      } else {
        newValues.interests = req.body.interests

        // Update MailChimp
        if (newValues.email !== '') {
          if (req.body.oldEmail === 'unsubscribed' || req.body.oldEmail === 'cleaned') req.body.oldEmail = null

          mailchimp.UpdateOrAddVisitor(req.body.oldEmail, newValues).then(function (result) {
            res.json(updatedVisitor)
          }).catch(function (err) {
            if (err.status === 400) {
              // Old email was bounced. Try again by making a new list member instead.
              mailchimp.UpdateOrAddVisitor(null, newValues).then(function (result) {
                res.json(updatedVisitor)
              }).catch(function (err) {
                // Failed again. Now for some creative error hadling...
                if (err.title === 'Member Exists') {
                  res.json(updatedVisitor)
                } else if (err.detail === 'Please provide a valid email address.') {
                  res.status(400).send(err.detail)
                } else {
                  console.error('Unhandled mailchimp error updating a visitor: ' + JSON.stringify(err, null, 4))
                  res.status(400).send(err)
                }
              })
            } else {
              console.error('Unhandled mailchimp error updating a visitor: ' + JSON.stringify(err, null, 4))
              res.status(500).send(err)
            }
          })
        } else res.json(updatedVisitor)
      }
    })
  },

  delete: function (req, res) {
    // Remove from DB (WARNING, EFFECT TO EVENT DATA NOT CONSIDERED!)
    Visitor.findByIdAndDelete(req._id, function (err, visitor) {
      if (err) {
        res.status(500).send(err)
      } else {
        mailchimp.DeleteVisitor(visitor).then(function (result) {
          res.status(200).send()
        }).catch(function (err) {
          if (err.status !== 404) console.error('Error deleting a visitor: ' + JSON.stringify(err, null, 4))
          res.status(200).send()
        })
      }
    })
  }
}
