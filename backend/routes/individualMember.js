const _ = require('lodash')
const crypto = require('crypto')
const emmiApi = require('./dbapi')
const config = require('../server').config

const mongoose = require('mongoose')
const individualMemberSchema = emmiApi.individualMemberSchema
const Member = mongoose.model('IndividualMember', individualMemberSchema)
const mailchimp = emmiApi.mailchimp
let stripe = null
if (!config.secrets.stripe) {
  const message = 'Stripe secrets not found! Payments will fail...'
  config.productionEnv ? console.error(message) : console.log(message)
} else {
  console.log('Initializing Stripe SDK...')
  stripe = require('stripe')(config.secrets.stripe.key)
}
const nodemailer = emmiApi.nodemailer
const slack = require('../slack')
// eslint-disable-next-line no-unused-vars
const csv = require('csv-express')

function CreateNewMember (res, memberData) {
  const newMember = new Member(memberData)
  newMember.email = newMember.email.toLowerCase().trim() // Just to be sure...
  newMember.createdDate = Date.now()
  newMember.validUntil = new Date()
  newMember.validUntil.setFullYear(newMember.validUntil.getFullYear() + 1, newMember.validUntil.getMonth() + 1, 0)
  newMember.validUntil.setHours(23, 59, 59)
  newMember.igdaOrgId = 'unknown'
  newMember.token = GetNewEditToken()

  // Get next free ID to use
  Member.find()
    .where('igdaFinlandId').gt(0)
    .sort('-igdaFinlandId')
    .limit(1)
    .select('igdaFinlandId')
    .exec(function (err, data) {
      if (err) {
        res.status(500).send(err)
      } else {
        newMember.igdaFinlandId = data[0].igdaFinlandId + 1

        // Create
        newMember.save(function (err, result) {
          if (err) {
            res.status(500).send(err)
          } else {
            nodemailer.sendMembershipCreatedMail(newMember)
            mailchimp.UpdateOrAddMember(newMember.email, newMember) // This returns a promise for future error handling if needed. Can't be bothered right now because it can't block the operation and mailchimp is self healing.
            res.json(result)
          }
        })
      }
    })
}

function SaveNewEditToken (_id, callback) {
  // Save token to DB
  Member.findByIdAndUpdate(_id, { $set: { token: GetNewEditToken() } }, { new: true }, function (err, updatedMember) {
    if (err) {
      callback(err, null)
    } else {
      callback(null, updatedMember.token)
    }
  })
}

function GetNewEditToken () {
  return crypto.createHash('md5').update(new Date().toString()).digest('hex')
}

module.exports = {
  getCount: function (req, res) {
    Member.countDocuments({}, function (err, data) {
      if (err) {
        res.status(500).send(err)
      } else {
        res.json({ count: data })
      }
    })
  },

  getAll: function (req, res) {
    Member.find(function (err, members) {
      if (err) {
        res.status(500).send(err)
      } else {
        res.json(members)
      }
    })
  },

  getAllCsv: function (req, res) {
    const query = Member.find()
    query.select('-_id firstName lastName email validUntil membershipType igdaFinlandId')
    query.lean()
    query.exec(function (err, members) {
      if (err) {
        res.status(500).send(err)
      } else {
        for (const member of members) {
          member.validUntil = new Date(member.validUntil)
          member.validUntil = `${member.validUntil.getFullYear()}-${member.validUntil.getMonth() + 1}-${member.validUntil.getDate() + 1}`
        }
        res.csv(members, true)
      }
    })
  },

  getAllValidAsCsv: function (req, res) {
    const query = Member.find({
      $or: [
        { validUntil: { $gte: new Date() } },
        { membershipType: 'Lifetime' }
      ]
    })
    query.select('-_id firstName lastName email validUntil membershipType igdaFinlandId')
    query.lean()
    query.exec(function (err, members) {
      if (err) {
        res.status(500).send(err)
      } else {
        for (const member of members) {
          member.validUntil = new Date(member.validUntil)
          member.validUntil = `${member.validUntil.getFullYear()}-${member.validUntil.getMonth() + 1}-${member.validUntil.getDate() + 1}`
        }
        res.csv(members, true)
      }
    })
  },

  getById: function (req, res) {
    Member.findById(req._id, function (err, member) {
      if (err) {
        res.status(500).send(err)
      } else {
        res.json(member)
      }
    })
  },

  getAllCards: function (req, res) {
    Member.find({}, '-_id firstName lastName createdDate validUntil membershipType igdaFinlandId', function (err, members) {
      if (err) {
        res.status(500).send(err)
      } else {
        res.json(members)
      }
    })
  },

  getAllValidCards: function (req, res) {
    Member.find({
      $or: [
        { validUntil: { $gte: new Date() } },
        { membershipType: 'Lifetime' }
      ]
    }, '-_id firstName lastName createdDate validUntil membershipType igdaFinlandId', function (err, members) {
      if (err) {
        res.status(500).send(err)
      } else {
        res.json(members)
      }
    })
  },

  getEditToken: function (req, res) {
    Member.findById(req._id, '-_id token', function (err, member) {
      if (err) {
        res.status(500).send(err)
      } else {
        if (member.token != null) {
          res.json(member.editToken.token)
        } else {
          // No token -> refresh
          SaveNewEditToken(req._id, function (err, res) {
            if (err) res.status(500).send(err)
            else res.json(res.token)
          })
        }
      }
    })
  },

  saveNewEditToken: SaveNewEditToken,

  isTokenValid: function (_id, token, callback) {
    Member.findById(_id, '-_id token', function (err, member) {
      if (err) {
        const error = 'Failed to find a member for edit token validation: ' + err
        console.error(error)
        slack.post('Failed to find a member for edit token validation:\n' + err, false)
        callback(error)
      } else {
        if (member == null) {
          // eslint-disable-next-line standard/no-callback-literal
          callback('MongoDB returned null member during isTokenValid. Has only ever happened in dev with broken links...')
          return
        }
        // Compare
        if (member.token === token) {
          // All good
          callback(null)
        } else {
          // Bad or used token
          // eslint-disable-next-line standard/no-callback-literal
          callback('Bad or used token')
        }
      }
    })
  },

  create: function (req, res) {
    CreateNewMember(res, req.body)
  },

  checkout: function (req, res) {
    if (stripe == null) {
      const message = 'Stripe not initialised! Failing payment...'
      config.productionEnv ? console.error(message) : console.log(message)
      if (config.productionEnv) slack.post('Got a webhook to process a payment without Stripe being initialised. Achtung!')
      res.status(500).send(message)
      return
    }

    let event
    const sig = req.headers['stripe-signature']
    try {
      event = stripe.webhooks.constructEvent(req.body, sig, config.secrets.stripe.endpointSecret)
    } catch (err) {
      console.error(`Webhook Error: ${err.message}`)
      res.status(400).send(`Webhook Error: ${err.message}`)
      return
    }

    const payload = JSON.parse(event.data.object.client_reference_id)

    if (payload === null) {
      res.status(400).send('Missing payload')
      return
    }

    if (event.type === 'checkout.session.completed') {
      if (payload.id) {
        // Renew an existing member
        Member.findById(payload.id, function (err, member) {
          if (err) {
            res.status(400).send(err)
          } else {
            const validUntil = new Date(member.validUntil)
            const now = new Date()
            if (validUntil < now) {
              validUntil.setFullYear(now.getFullYear() + 1, now.getMonth() + 1, 0)
              validUntil.setHours(23, 59, 59)
            } else {
              validUntil.setFullYear(validUntil.getFullYear() + 1, validUntil.getMonth() + 1, 0)
              validUntil.setHours(23, 59, 59)
            }

            member.membershipType = payload.membershipType
            member.validUntil = validUntil
            member.lastRenewed = new Date()
            member.save(function (err, updatedMember) {
              if (err) {
                console.error('Error saving a renewed member: ' + err)
                slack.post('Error saving a renewed member:\n' + err, false)
                res.status(500).send(err)
              } else {
                nodemailer.sendMembershipRenewedMail(member)
                slack.post(member.firstName + ' ' + member.lastName + ' just renewed their membership.\nMembership type: ' + member.membershipType, true)
                res.json({
                  membershipType: payload.membershipType,
                  validUntil
                })
              }
            })
          }
        })
      } else if (payload.member) {
        // Create a new member
        slack.post(payload.member.firstName + ' ' + payload.member.lastName + ' joined IGDA Finland.\nMembership type: ' + payload.member.membershipType, true)
        CreateNewMember(res, payload.member)
      } else {
        const message = `Malformed payload: ${event.client_reference_id}`
        console.error(message)
        return res.status(400).send(message)
      }
    } else {
      res.status(400).send('Unhandled')
    }
  },

  update: function (req, res) {
    var newData = req.body
    newData.email = newData.email.toLowerCase().trim() // Just to be sure...
    newData.lastUpdated = new Date()

    Member.findByIdAndUpdate(req._id, { $set: newData }, { new: true }, function (err, updatedMember) {
      if (err) {
        res.status(500).send(err)
      } else {
        mailchimp.UpdateOrAddMember(updatedMember.email, updatedMember)

        res.json(updatedMember)
      }
    })
  },

  updateWithToken: function (req, res) {
    req.body.email = req.body.email.toLowerCase().trim() // Just to be sure...
    // Fetch current email and compare
    let sendNewTokenEmail = false
    Member.findById(req._id, function (err, member) {
      if (err) {
        console.error('Error while fetching a member for updating with token:' + err)
        slack.post('Error while fetching a member for updating with token:\n' + err, false)
        res.status(500).send(err)
      } else {
        let oldEmail = ''
        if (req.body.email !== member.email) {
          // Email changed -> invalidate token
          req.body.token = GetNewEditToken()
          sendNewTokenEmail = true
          oldEmail = member.email
        }

        // Cherrypick only the data that members can update by themselves
        member.firstName = req.body.firstName
        member.lastName = req.body.lastName
        member.email = req.body.email
        member.postOffice = req.body.postOffice
        member.token = req.body.token
        member.lastUpdated = new Date()

        member.save(function (err, data) {
          if (err) {
            console.error('Error saving a token-edited member: ' + err)
            slack.post('Error saving a token-edited member:\n' + err, false)
            res.status(500).send()
          } else {
            if (oldEmail === '') mailchimp.UpdateOrAddMember(member.email, member)
            else mailchimp.UpdateOrAddMember(oldEmail, member)

            if (sendNewTokenEmail) {
              nodemailer.sendNewAddressEmail(member)
            }
            res.json(data)
          }
        })
      }
    })
  },

  delete: function (req, res) {
    Member.findByIdAndRemove(req._id, function (err, member) {
      if (err) {
        res.status(500).send(err)
      } else {
        mailchimp.DeleteMember(member)
        res.status(200).send()
      }
    })
  },

  sendMonthlyEmails: function () {
    const now = new Date()
    const inOneMonth = new Date()
    const oneMonthAgo = new Date()
    inOneMonth.setMonth(now.getMonth() + 1)
    oneMonthAgo.setMonth(now.getMonth() - 1)

    // Get all non-lifetime members that are about to expire during the next month
    Member.find({
      validUntil: { $gte: now, $lt: inOneMonth },
      membershipType: { $ne: 'Lifetime' }
    }, function (err, members) {
      if (err) {
        console.error('Error fetching future expiring members: ' + err)
        slack.post('Error fetching future expiring members:\n' + err, false)
      } else {
        console.log('Sending renewal reminder emails to ' + members.length + ' members')
        slack.post('Sending renewal reminder emails to ' + members.length + ' members.', false)
        _.forEach(members, function (member) {
          if (member.token === null) {
            SaveNewEditToken(member._id, function (err, token) {
              if (err) {
                console.error('Error saving a new edit token ' + err)
                slack.post('Error saving a new edit token.\n' + err, false)
              } else {
                member.token = token
                nodemailer.sendMembershipAboutToExpireMail(member)
              }
            })
          } else nodemailer.sendMembershipAboutToExpireMail(member)
        })
      }
    })

    // Get all non-lifetime members that exired during the past month
    Member.find({
      validUntil: { $gte: oneMonthAgo, $lt: now },
      membershipType: { $ne: 'Lifetime' }
    }, function (err, members) {
      if (err) {
        console.error('Error fetching expired members: ' + err)
        slack.post('Error fetching expired members:\n' + err, false)
      } else {
        console.log('Sending expiration emails to ' + members.length + ' members')
        slack.post('Sending expiration emails to ' + members.length + ' members.', false)
        _.forEach(members, function (member) {
          if (member.token === null) {
            SaveNewEditToken(member._id, function (err, token) {
              if (err) {
                console.error('Error saving a new edit token ' + err)
                slack.post('Error saving a new edit token.\n' + err, false)
              } else {
                member.token = token
                nodemailer.sendMembershipExpiredMail(member)
              }
            })
          } else nodemailer.sendMembershipExpiredMail(member)
        })
      }
    })
  }
}
