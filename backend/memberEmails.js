const nodemailer = require('nodemailer')
const mailgun = require('nodemailer-mailgun-transport')
const server = require('./server')
const config = server.config
let transporter = null

// Handle missing secrets
if (!config.secrets.hasOwnProperty('nodemailer')) {
  const message = 'Mailgun secrets not found! Emails disabled...'
  config.productionEnv ? console.error(message) : console.log(message)
} else {
  transporter = nodemailer.createTransport(mailgun(config.secrets.nodemailer))

  // Check connection
  transporter.verify(function (err, success) {
    if (err) {
      console.error('Nodemailer connection verification error: ' + err)
    } else {
      console.log('Nodemailer connection verified!')
    }
  })
}

function DoubleCheckEmailAddress (to) {
  if (config.productionEnv || to.includes('@igda.fi') || to.includes('teemu.haila')) {
    return true
  } else {
    return false
  }
}

function SendEmail (options, callback) {
  callback = callback || function () { }
  if (!DoubleCheckEmailAddress(options.to)) {
    console.log('Blocked sending an email outside of the igda.fi domain from DEV. May cause unhandled errors...')
    let err = 'Prevented sending an email from DEV environment to ' + options.to
    callback(err)
  } else {
    if (!config.productionEnv) {
      options.subject += ' (DEV)'
      options.text = '------- TEST EMAIL FROM EMMI DEVELOPMENT ENVIRONMENT -------\n\n' + options.text
    } else {
      options.bcc = 'members@igda.fi'
    }

    if (transporter != null) {
      transporter.sendMail(options, function (err, info) {
        if (err) {
          let message = "Nodemailer email sending error for '" + options.subject + "' to '" + options.to + "': " + err
          console.error(message)
          callback(message)
        } else {
          console.log('Email "' + options.subject + '" sent to ' + options.to)
          callback(null)
        }
      })
    } else {
      console.log('Skipped sending "' + options.subject + '" because nodemailer is not initialised.')
      callback(null)
    }
  }
}

// Debug hook
module.exports.sendDebugEmail = function (req, res) {
  // req.body.member, req.body.type = new/willExpire/expired/renewed/editToken
  req.body.member.validUntil = new Date(req.body.member.validUntil)
  switch (req.body.type) {
    case 'new':
      module.exports.sendMembershipCreatedMail(req.body.member, function (err) {
        if (err) res.status(500).send(err)
        else res.send('ok')
      })
      break
    case 'willExpire':
      module.exports.sendMembershipAboutToExpireMail(req.body.member, function (err) {
        if (err) res.status(500).send(err)
        else res.send('ok')
      })
      break
    case 'expired':
      module.exports.sendMembershipExpiredMail(req.body.member, function (err) {
        if (err) res.status(500).send(err)
        else res.send('ok')
      })
      break
    case 'renewed':
      module.exports.sendMembershipRenewedMail(req.body.member, function (err) {
        if (err) res.status(500).send(err)
        else res.send('ok')
      })
      break
    case 'editToken':
      module.exports.sendIndividualLinksEmail(req.body.member, function (err) {
        if (err) res.status(500).send(err)
        else res.send('ok')
      })
      break
    case 'newAddress':
      module.exports.sendNewAddressEmail(req.body.member, function (err) {
        if (err) res.status(500).send(err)
        else res.send('ok')
      })
      break
    default:
      console.error('Debug email error: ' + JSON.stringify(req.body, null, 3))
      res.status(500).json({ message: 'Infernal Server Terror! Check the logs :(', body: req.body })
      break
  }
}

function getValidUntil (member) {
  var monthNames = [
    'January', 'February', 'March',
    'April', 'May', 'June', 'July',
    'August', 'September', 'October',
    'November', 'December'
  ]

  var monthIndex = member.validUntil.getMonth()
  var year = member.validUntil.getFullYear()

  return (member.membershipType === 'Lifetime') ? 'Forever!' : monthNames[monthIndex] + ' ' + year
}

function getEditLink (member) {
  if (member.token == null) {
    console.error('Tried to generate an edit link without a token. This should never happen in production.')
    return 'undefined'
  }

  if (config.productionEnv) return 'https://emmi.igda.fi/individual-members/' + member._id + '/' + member.token
  else return 'http://localhost:8080/individual-members/' + member._id + '/' + member.token
}

function getIndividualCardLink (member) {
  if (config.productionEnv) return `https://emmi.igda.fi/card/individual/${member._id}`
  else return `http://localhost:8080/card/individual/${member._id}`
}

function getAffiliateCardLink (affiliateMember) {
  if (config.productionEnv) return `https://emmi.igda.fi/card/affiliate/${affiliateMember._id}`
  else return `http://localhost:8080/card/affiliate/${affiliateMember._id}`
}

// Emails
module.exports.sendMembershipCreatedMail = function (member, callback) {
  let mailOptions = {
    from: config.nodemailer.from.members,
    to: member.email,
    subject: 'Welcome to IGDA Finland!',
    text: ['Hello ' + member.firstName + ',',
      '',
      'Welcome to IGDA Finland, one of the leading chapters of the International Game Developers Association! Your membership helps promote game development and the advancement of game related careers in Finland and around the world.',
      '',
      'Thank you for your support!',
      '',
      'Membership number: ' + member.igdaFinlandId,
      'Membership type: ' + member.membershipType,
      'Valid until: ' + getValidUntil(member),
      '',
      'View your digital membership card: ' + getIndividualCardLink(member),
      '',
      'Best regards,',
      '- IGDA Finland Team'].join('\n')
    // html: '<b>Hello world 🐴</b>' // html body
  }

  return SendEmail(mailOptions, callback)
}

module.exports.sendMembershipExpiredMail = function (member, callback) {
  let mailOptions = {
    from: config.nodemailer.from.members,
    to: member.email,
    subject: 'Oh no! IGDA Finland Membership Expired',
    text: ['Hello ' + member.firstName + ',',
      '',
      'We just wanted to let you know that your individual IGDA Finland Ry membership has expired. We appreciate your continued support to the Finnish game community so please consider renewing your membership or helping with our many events.',
      '',
      "Note: This doesn't affect any studio memberships you might be a part of.",
      '',
      'Renew now: ' + getEditLink(member),
      '',
      'Membership number: ' + member.igdaFinlandId,
      'Membership type: ' + member.membershipType,
      'Expired: ' + getValidUntil(member),
      '',
      'Best regards,',
      '- IGDA Finland Team'].join('\n')
    // html: '<b>Hello world 🐴</b>' // html body
  }

  return SendEmail(mailOptions, callback)
}

module.exports.sendMembershipAboutToExpireMail = function (member, callback) {
  let mailOptions = {
    from: config.nodemailer.from.members,
    to: member.email,
    envelope: {},
    subject: 'IGDA Finland Membership Expiring Soon',
    text: ['Hello ' + member.firstName + ',',
      '',
      'Your individual IGDA Finland Ry membership will expire soon. Renewing your membership will help us cover IGDA Finland’s modest operating expenses and help promote game development in Finland.',
      '',
      "Note: This doesn't affect any studio memberships you might be a part of.",
      '',
      'Renew now: ' + getEditLink(member),
      '',
      'Membership number: ' + member.igdaFinlandId,
      'Membership type: ' + member.membershipType,
      'Valid until: ' + getValidUntil(member),
      '',
      'Best regards,',
      '- IGDA Finland Team'].join('\n')
    // html: '<b>Hello world 🐴</b>' // html body
  }

  return SendEmail(mailOptions, callback)
}

module.exports.sendMembershipRenewedMail = function (member, callback) {
  let mailOptions = {
    from: config.nodemailer.from.members,
    to: member.email,
    subject: 'IGDA Finland Membership Renewed!',
    text: ['Hey ' + member.firstName + ',',
      '',
      "Your IGDA membership has been renewed! You continue making our volunteers' lives easier and we salute you for your generosity. Respect.",
      '',
      'Membership number: ' + member.igdaFinlandId,
      'Membership type: ' + member.membershipType,
      'Valid until: ' + getValidUntil(member),
      '',
      'View your digital membership card: ' + getIndividualCardLink(member),
      '',
      'Best regards,',
      '- IGDA Finland Team'].join('\n')
    // html: '<b>Hello world 🐴</b>' // html body
  }

  return SendEmail(mailOptions, callback)
}

module.exports.sendIndividualLinksEmail = function (member, callback) {
  let mailOptions = {
    from: config.nodemailer.from.members,
    to: member.email,
    subject: 'IGDA Finland Membership Links',
    text: ['Hey ' + member.firstName + ',',
      '',
      'Someone requested your membership links to be emailed to this address. We hope it was you. If not feel free to ignore this email and/or report the abuse to us.',
      '',
      'Update your membership profile: ' + getEditLink(member),
      'View your digital membership card: ' + getIndividualCardLink(member),
      '',
      'Best regards,',
      '- IGDA Finland Team'].join('\n')
    // html: '<b>Hello world 🐴</b>' // html body
  }

  SendEmail(mailOptions, callback)
}

module.exports.sendAffiliateLinksEmail = function (affiliate, callback) {
  let mailOptions = {
    from: config.nodemailer.from.members,
    to: affiliate.members[0].email,
    subject: 'IGDA Finland Membership Links',
    text: ['Hey ' + affiliate.members[0].firstName + ',',
      '',
      'Someone requested your membership card link to be emailed to this address. We hope it was you. If not feel free to ignore this email and/or report the abuse to us.',
      '',
      'View your digital membership card: ' + getAffiliateCardLink(affiliate.members[0]),
      '',
      'Note: if you would like to get your affiliate membership details updated (like your name, for example) just reply to this email and let us know!',
      '',
      'Best regards,',
      '- IGDA Finland Team'
    ].join('\n')
    // html: '<b>Hello world 🐴</b>' // html body
  }

  SendEmail(mailOptions, callback)
}

module.exports.sendNewAddressEmail = function (member, callback) {
  let mailOptions = {
    from: config.nodemailer.from.members,
    to: member.email,
    subject: 'IGDA Finland Membership Email Updated',
    text: ['Hey ' + member.firstName + ',',
      '',
      "Your email address has been changed successfully. For security reasons here's a new membership update link for you to use: " + getEditLink(member),
      '',
      'Best regards,',
      '- IGDA Finland Team'].join('\n')
    // html: '<b>Hello world 🐴</b>' // html body
  }

  SendEmail(mailOptions, callback)
}

module.exports.sendCardOrderEmail = function (to, count, link, callback) {
  let mailOptions = {
    from: config.nodemailer.from.members,
    to: to,
    subject: 'New IGDA Membership Cards',
    text: ['Hey,',
      '',
      "Here's a CSV file with the order details for " + count + ' new cards: ' + link,
      '',
      'Best regards,',
      '- IGDA Finland Team'].join('\n')
    // html: '<b>Hello world 🐴</b>' // html body
  }

  SendEmail(mailOptions, callback)
}
