# Backend Overview

## Context

Originally this project was formed around a very simple API consisting of not much more than an event and visitor database with some admin tools. Over time it has grown to handle our membership workflows and a significant amount of automated communication like email list syncing. Non-systematic feature creep has left it's marks but enough refactoring has been done to keep most code paths fairly isolated: breaking something in one doesn't generally break anything else.

This have changed since, tho, and any contributions to modernise the general structure one piece at a time would be very helpful. Also the API has stabilised enough that starting work on tests would be a very valuable addition.

## Where to start

`node server.js` runs the whole thing. Going through server.js is a good way to understand roughly what pieces make up the whole system and it works as a kind of service map at this point. After that routes/dbapi.js is the heart of actual request handling with dm schema definitions at the top for convenience. Individual API route handling is then split in to their own files based on general cathegory like events or members.

Some code duplication exists in routes and that's absolutely not desireable. It's also not a problem at this point. General philosophy has been to avoid data transformations both when reading and writing to the DB so the majority of requests just pass through both ways as-is. It shifts most resposibility of data structuring to the client and that's been fine since only authorised users can edit any data and there's no incentive for intentionally feeding malicious data to the DB. Nightly backups provide piece of mind.