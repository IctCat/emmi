const server = require('./server')
const config = server.config
const request = require('request')

module.exports.post = function (message, isPrivate, attachments, callback) {
  if (!config.secrets.slack) {
    const message = 'Slack secrets not found! Skipping message...'
    config.productionEnv ? console.error(message) : console.log(message)
    return
  }
  if (!config.productionEnv) return

  const slackUrl = isPrivate ? config.secrets.slack.slackUrlPrivateChannel : config.secrets.slack.slackUrlPublicChannel

  const slackRequest = { text: message }
  if (attachments) request.attachments = attachments

  request.post(slackUrl, { json: slackRequest }, function (err, response, body) {
    if (err) {
      console.error("Failed posting '" + message + "' to slack: " + response)
    } else {
      console.log("Posted '" + message + "' to slack")
    }
    if (callback != null) callback(err, response)
  })
}
