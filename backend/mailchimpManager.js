const Promise = require('bluebird')

// http://developer.mailchimp.com/documentation/mailchimp/guides/manage-subscribers-with-the-mailchimp-api/
// http://developer.mailchimp.com/documentation/mailchimp/reference/lists/members/
// Init
const md5 = require('md5')
const serverConfig = require('./server').config
let config = null
const MailchimpApi = require('mailchimp-api-v3')
let mailchimp = null

// Handle missing secrets
if (!serverConfig.secrets.mailchimp) {
  const message = 'Mailchimp secrets not found! Email lists disabled...'
  serverConfig.productionEnv ? console.error(message) : console.log(message)
} else {
  config = serverConfig.secrets.mailchimp
  mailchimp = new MailchimpApi(config.key)
}

// Exports
module.exports.AddVisitor = function (visitor) {
  if (mailchimp == null) return 'Mailchimp not initialised'

  return mailchimp.post('/lists/' + config.visitorList + '/members/', {
    status: 'pending', // Sends an opt-in email
    email_address: visitor.email,
    merge_fields: { FNAME: visitor.firstName, LNAME: visitor.lastName },
    interests: visitor.interests
  })
}

module.exports.UpdateOrAddVisitor = function (oldEmail, visitor) {
  if (mailchimp == null) return 'Mailchimp not initialised'

  const request = {
    status: 'subscribed',
    email_address: visitor.email,
    merge_fields: { FNAME: visitor.firstName, LNAME: visitor.lastName }
  }
  if (visitor.interests) { request.interests = visitor.interests }

  if (oldEmail != null) return mailchimp.put('/lists/' + config.visitorList + '/members/' + md5(oldEmail), request)
  else return mailchimp.post('/lists/' + config.visitorList + '/members', request)
}

module.exports.GetVisitor = function (email) {
  return new Promise(function (resolve, reject) {
    if (mailchimp == null) {
      resolve({ status: 'not initialised' })
    // Check if email is not already marked something weird
    } else if (email === 'cleaned') {
      resolve({ status: 'cleaned' })
    } else {
      // Get visitor info
      mailchimp.get('/lists/' + config.visitorList + '/members/' + md5(email)).then(function (result) {
        // Get list groups
        mailchimp.get('/lists/' + config.visitorList + '/interest-categories/' + config.visitorGroupId + '/interests').then(function (result2) {
          // Combine names and subscription statuses
          result.namedInterests = []
          result2.interests.forEach(group => {
            result.namedInterests.push({
              id: group.id,
              name: group.name,
              status: result.interests[group.id]
            })
          })
          resolve(result)
        }).catch(function (err) {
          reject(err)
        })
        // resolve(result);
      }).catch(function (err) {
        reject(err)
      })
    }
  })
}

module.exports.DeleteVisitor = function (visitor) {
  if (mailchimp == null) return 'Mailchimp not initialised'

  const emailHash = md5(visitor.email)
  return mailchimp.delete('/lists/' + config.visitorList + '/members/' + emailHash)
}

module.exports.GetInterests = function () {
  return new Promise(function (resolve, reject) {
    if (mailchimp == null) {
      resolve([])
    // Check if email is not already marked something weird
    } else {
      mailchimp.get('/lists/' + config.visitorList + '/interest-categories/' + config.visitorGroupId + '/interests').then(function (result) {
        const groups = []
        result.interests.forEach(group => {
          groups.push({
            name: group.name,
            id: group.id
          })
        })
        resolve(groups)
      }).catch(function (err) {
        reject(err)
      })
    }
  })
}

/* module.exports.AddMember = function (member) {
    return mailchimp.post('/lists/' + config.memberList + "/members/", {
        status: "subscribed", // No opt-in email
        email_address: member.email,
        merge_fields: { FNAME: member.firstName, LNAME: member.lastName }
    });

} */

module.exports.UpdateOrAddMember = function (oldEmail, member) {
  if (mailchimp == null) return 'Mailchimp not initialised'

  return mailchimp.put('/lists/' + config.memberList + '/members/' + md5(oldEmail), {
    status_if_new: 'subscribed',
    email_address: member.email,
    merge_fields: { FNAME: member.firstName, LNAME: member.lastName }
  })
}

module.exports.GetMember = function (email) {
  return new Promise(function (resolve, reject) {
    if (mailchimp == null) {
      resolve({ status: 'not initialised' })
    // Check if email is not already marked something weird
    } else if (email === 'cleaned') {
      resolve({ status: 'cleaned' })
    } else {
      // Get info
      mailchimp.get('/lists/' + config.memberList + '/members/' + md5(email)).then(function (result) {
        resolve(result)
      }).catch(function (err) {
        reject(err)
      })
    }
  })
}

module.exports.DeleteMember = function (member) {
  if (mailchimp == null) return 'Mailchimp not initialised'

  const emailHash = md5(member.email)
  return mailchimp.delete('/lists/' + config.memberList + '/members/' + emailHash)
}
